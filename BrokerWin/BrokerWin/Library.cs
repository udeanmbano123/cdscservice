﻿using BrokerWin.DDO;
using BrokerWin.DealClass;
using BrokerWin.DealNotes;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BrokerWin
{
    public static class Library
    {
        public static BackEndCDSEntities backendCDSEntities = new BackEndCDSEntities();


        public static void WriteErrorLog(string message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ":" + message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {


            }

        }

    
        public static void PostCharges()
        {

            try
            {
                // flag transactions to true

                var paymentslist = backendCDSEntities.Transfers.ToList().Where(a =>a.processed==true&&a.Success==true&&a.Reference== "SUBSCRIPTION"&&a.Flag==false);
                string pay, went;
                foreach(var pays in paymentslist)
                {
                    var tr = backendCDSEntities.Transfers.Find(pays.id);
                    tr.Flag = true;
                    backendCDSEntities.Transfers.AddOrUpdate(tr);

                    //CommittableTransaction if successful
                    if (tr.processed==true&&tr.Success==true)
                    {
                        pay = "PAID";
                        went = "SUCCESS";
                    }
                    else
                    {
                        pay = "NPAID";
                        went = "NSUCCESS";
                    }

                    //Update Customer record
                    var up = backendCDSEntities.Subscribers.Where(a => a.PhoneNumber.Trim() == tr.cdsnumber.Trim()).FirstOrDefault();

                    var my = backendCDSEntities.Subscribers.Find(up.Id);
                    my.runnS = pay;
                    my.Status = went;
                    my.CheckoutRequestID = tr.id.ToString();
                    if (went == "SUCCESS")
                    {
                        my.DateTime = DateTime.Now;
                        my.subss = my.DateTime.AddYears(1);
                    }
                    else
                    {
                        my.DateTime = my.DateTime;
                        my.subss = my.subss;
                    }
                    backendCDSEntities.Subscribers.AddOrUpdate(my);
                  
                }
                backendCDSEntities.SaveChanges();

            }
            catch (Exception f)
            {

                WriteErrorLog(f.Message);
            }
           
        }
      
    }  
        }
    


