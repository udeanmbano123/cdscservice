//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerWin.DDO
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transfer
    {
        public decimal id { get; set; }
        public string account_number { get; set; }
        public string expiry { get; set; }
        public string pin { get; set; }
        public Nullable<bool> processed { get; set; }
        public string Response { get; set; }
        public Nullable<bool> Success { get; set; }
        public Nullable<bool> Credit_Check { get; set; }
        public Nullable<decimal> Credit_Balance { get; set; }
        public string Credit_Response { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Reference { get; set; }
        public string cdsnumber { get; set; }
        public Nullable<System.DateTime> transferDate { get; set; }
        public Nullable<bool> Flag { get; set; }
    }
}
